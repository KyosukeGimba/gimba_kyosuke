<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<head>
    <link href="./css/signup.css" rel="stylesheet" type="text/css">
</head>
<br>
<div class="signup">ユーザー新規登録</div>

		<div class="header">
			<a href="logout" id="href">ログアウト</a><br>
		</div>

		<a href="userManagement" id="href">ユーザー管理</a>
		<br><br>
		<a href="./" id="href" type="button">ホームに戻る</a>


		<c:if test="${ empty errorMessages }"><br></c:if>

        <c:if test="${ not empty errorMessages }" >
                <div class="errorMessages">
              	  <ul style = "display: inline-block; text-align:left">
                        <c:forEach items="${errorMessages}" var="message">
                      		 <c:out value="${message}" /><br>
                        </c:forEach>
                   </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

			<div class="main-contents">
            	<form action="signup" method="post">
            		 <label for="name">名称</label> <br />
            			 <input name="name" id="name" value="${user.name}" placeholder="10文字以下"/>
            <br />
            		<label for="login_id">ログインID(半角英数字)</label> <br />
						<input name="login_id" id="login_id"  value="${user.login_id}" placeholder="6文字以上20文字以下" />
            <br />
					<label for="password1">パスワード(半角英数字記号)</label> <br />
						<input name="password1" type="password" id="password1" placeholder="6文字以上20文字以下"/>
			<br />
					<label for="password2">パスワード確認用</label> <br />
						<input name="password2" type="password" id="password2" placeholder=""/> <br />


							<!-- 入力した支店、部署などの値の保持 -->
  			<span>
  				<c:if test="${branch.id == 0}">
  					<label for="branch_id">支店</label> <br />
						<select name="branch">
							<option value="0" selected="selected" >選択してください
					<!-- <input type="hidden" name="0" value="0" /> --></option>
						<c:forEach items="${branchList}" var="branch">
							<option value="${branch.id}" >${branch.name}</option>
						</c:forEach>
					</select>
				</c:if>
			</span>


  			<span><label for="branch_id">支店</label> <br />
				<select name="branch">
					<option value="0" >選択してください	</option>
					<!-- <input type="hidden" name="0" value="0" /> -->
					<c:forEach items="${branchList}" var="branch">
						<c:if test="${branch.id == user.branch_id}">
						<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>

						<c:if test="${branch.id != user.branch_id}">
							<option value="${branch.id}">${branch.name}</option>
						</c:if>
					</c:forEach>
				</select>
			</span>

			<c:if test="${position.id == 0}">
  				<span><label for="position_id">部署･役職</label> <br />
					<select name="position">
							<option value="0" selected="selected" >選択してください
					<!-- <input type="hidden" name="0" value="0" /> --></option>
						<c:forEach items="${positionList}" var="position">
							<option value="${position.id}" >${position.name}</option>
						</c:forEach>
					</select>
				</span>
			</c:if>

  					<br /> <label for="position_id">部署･役職</label> <br />
						<select name="position" >
								<option value="0" >選択してください</option>
							<c:forEach items="${positionList}" var="position">
							<c:if test="${position.id == user.position_id}">
								<option value="${position.id}" selected>${position.name}</option>
							</c:if>
							<c:if test="${position.id != user.position_id}">
								<option value="${position.id}">${position.name}</option>
							</c:if>
							</c:forEach>
						</select><br /><br />

                <input type="submit" value="登録"  id="submit_button"/> <br />
                </form>
   </div>
                <br>

            <div class="copyright">Copyright(c)Kyosuke Gimba</div>



</body>
</html>