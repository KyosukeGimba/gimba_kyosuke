<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
</head>
<head>
    <link href="./css/home.css" rel="stylesheet" type="text/css">
</head>

<br><div class="publish">社内掲示板</div>

	<div class="header">
         <a href="logout" id="href" type="button">ログアウト</a>
     </div>

     <span><a href="newPublish" id="href" type="button">新規投稿</a></span>
      <br>
     <!--  総務部しか表示されない -->
     <c:if test="${loginUser.position_id == 1}">
        <br><a href="userManagement" id="href" type="button">ユーザー管理</a><br>
     </c:if><br>

    <!--  <div class="company">社内掲示板</div> -->


<body>
 		 <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                        <c:forEach items="${errorMessages}" var="message">
                            <c:out value="${message}" />
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session"/>
      	   </c:if><br>


        <span class="welcome"> <c:out value="${loginUser.name}"></c:out>さん、ようこそ</span>
        <br>

<%-- 		<p class="form-area">
			 <c:if test="${ isShowMessageForm }">
			 	 <form action="newMessage" method="post">
					<textarea name="message" cols="100" rows="5"  class="tweet-box"></textarea> <br />
			 			 <input type="submit" value="投稿">
				 </form>
			</c:if>
		</p>
		 <br> --%>

		    <!-- 検索エリア -->
			<br><p class="searchCategory"><form action="./" method="get">カテゴリー
				<input type="search" name="searchCategory" id="searchCategory" placeholder="キーワードを入力"  value="${searchCategory}" ><br>
				<div class="search-area">
				期間<input type="date" name="firstDate" id="firstDate"  value="${firstDate}" />～
				<input type="date" name="finalDate" id="finalDate"  value="${finalDate}" />
				<input type="submit" value="検索"  id="submit_button"/></div>
			<!-- 	<input type="reset" value="リセット" /> -->
			</form>
		<br /><br>


	<!-- 投稿表示 --><br><br>
	<div class ="publishlist"><label for="publish">投稿一覧</label></div>
		<c:forEach items="${newPublishes}" var="newPublish">
		<div class="text">
			<p><div class="title"><br><p><label for="title" id="title">タイトル</label></p>
					<c:out value="${newPublish.title}"/>
				</div>
			<br />

			<p><span class="line-break1" ><c:out value="${newPublish.text}"></c:out></span></p>
			<br>
			   	<span class="category">カテゴリー<br>
    				<c:out value="${newPublish.category}" /></span><br><br>
			<br>
			<span class="name" >published by <c:out value="${newPublish.name}" /><br>
			<fmt:formatDate value="${newPublish.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></span><br>


			<!-- 投稿削除 -->
		<div class="button">
			<c:if test="${loginUser.id == newPublish.userid || loginUser.position_id == 2}">
				<form action="PublishDelete" method="post">
			 		<input type="hidden" name= "publish_id"  value="${newPublish.id}" />
					<input type="submit" value="上記の投稿を削除" onClick="return disp();" id="submit_button"/>
				</form><br>
			</c:if>
		</div>


		<!-- コメント表示 -->
		<div class="comment">
			<c:forEach items="${comments}" var="UserComment">
				<c:if test="${UserComment.publishId == newPublish.id}">
					<br><br><label for="comme">コメント</label><br><br>

					<span class="line-break"><c:out value="${UserComment.text}"/>
					</span>
					<span>commented by <c:out value= "${UserComment.name}" /></span><br>
					<span><fmt:formatDate value="${UserComment.created_date}" pattern="yyyy/MM/dd HH:mm:ss"/>
					</span>
				</c:if>

				<!-- コメント削除 -->
				 <c:if test="${loginUser.id == UserComment.userId}">
				 <c:if test="${UserComment.publishId == newPublish.id}">
					<div class= "CommentDelete">
						<form action="CommentDelete" method="post">
							 <input type="hidden" name= "comment_id"  value="${UserComment.id}" />
							 <input type="submit" value="削除" onClick="return disp1();" id="submit_button"/>
							 <br><br>
						</form>
					</div>
				</c:if>
				</c:if>
			 </c:forEach>
		</div>
		<!-- コメント投稿 -->
			<div class="form-area">
			<form action="comment" method="post">
				 <c:if test="${newPublish.id != inputUserComment.publishId}">
					<textarea name="comment" cols="25" rows="5" class="tweet-box" ></textarea><br />
		          	 	<input type="hidden" name= "publish_id"  value="${newPublish.id}" />
		          	 	<input type="hidden" name= "user_id"  value="${loginUser.id}" />
		         	 	<input type="submit" value="コメント" id="submit_button"/> <br />(500文字以内)
		    	 </c:if>

			</form>
			</div>

			<div class="form-area">
				<form action="comment" method="post">
					<c:if test="${newPublish.id == inputUserComment.publishId}">
						<textarea name="comment" cols="25" rows="5" class="tweet-box">${inputUserComment.text}</textarea><br />
		         		   <input type="hidden" name= "publish_id"  value="${newPublish.id}" />
		          		   <input type="hidden" name= "user_id"  value="${loginUser.id}" />
		          		   <input type="submit" value="コメント" id="submit_button"/> <br />(500文字以内)
		          			  <c:remove var="inputUserComment" scope="session"/>
		     	   </c:if>
				</form>
			</div></div>
		 </c:forEach>

		<br>
	   <a href="./" id="href">上に戻る</a>
            <div class="copyright"> Copyright(c)Kyosuke Gimba</div>

<script type="text/javascript">
<!--
function disp(){
	if(window.confirm('本当にこの投稿を削除しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
function disp1(){
	if(window.confirm('本当にこのコメントを削除しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
// -->
</script>
    </body>
</html>