<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<head>
    <link href="./css/publish.css" rel="stylesheet" type="text/css">
</head>
<br>
	<div class="newPublish">新規投稿</div>

	<div class="header">
		<a href="logout" id="href">ログアウト</a>
	</div>


<a href="./" id="href">ホームに戻る</a><br>


		     <c:if test="${empty errorMessages }"><br></c:if>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                	<ul style = "display: inline-block; text-align:left">
                        <c:forEach items="${errorMessages}" var="message">
                            <c:out value="${message}" /><br>
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>


     <div class="main-contents">
			<form action="newPublish" method="post">
                <label for="title">タイトル</label> (30文字以下)<br />
               <span> <input name="title" id="title" value="${newPublish.title}"/></span> <br />

                <label for="text">本文</label>（1000文字以下）<br />

                <span><textarea name="text" cols="100" rows="5" class="tweet-box">${newPublish.text}</textarea></span>
               <br />

                <label for="category">カテゴリー</label>（10文字以下）<br />
               <span> <input name="category" type="text" id="category" value="${newPublish.category}"/></span>
                <br />
                <input type="submit" value="投稿" id="submit_button"/><br />
            </form>
        </div>

            <div class="copyright"> Copyright(c)Kyosuke Gimba</div>

</html>