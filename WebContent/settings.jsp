<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
    <%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
</head>
<head>
    <link href="./css/settings.css" rel="stylesheet" type="text/css">
</head>
<br>
<div class="useredit" >ユーザー編集</div>

	<div class="header">
 		<a href="logout" id="href" >ログアウト</a>
 	</div>

	<a href="userManagement" id="href" >ユーザー管理</a>
<!--  	<br><br>
 	<a href="signup" id="href" type="button">ユーザー新規登録</a> -->



		<c:if test="${ empty errorMessages }"><br><br></c:if>

        <c:if test="${ not empty errorMessages }">
           <div class="errorMessages">
           		 <ul style = "display: inline-block; text-align:left">
                      <c:forEach items="${errorMessages}" var="message">
                          <c:out value="${message}" /><br>
                     </c:forEach>
                 </ul>
            </div>
             <c:remove var="errorMessages" scope="session"/>
        </c:if>

             <div class="main-contents">
             	<form action="settings" method="post">
           			 <input type="hidden" name="before_id" value="${before_id}"/>

            		 <input name="id" value="${editUser.id}" id="id" type="hidden"/>
            			<label for="name">名称</label><br />
					<input name="name" value="${editUser.name}" id="name" placeholder="10文字以下で入力"/><br />

            			<label for="login_id">ログインID(半角英数字)</label>
            		<input name="login_id" value="${editUser.login_id}" placeholder="6文字以上20文字以下"/><br />

              			 <label for="password1">パスワード(半角英数字記号)</label><input
                    name="password1" type="password" id="password1" placeholder="6文字以上20文字以下"/><br />

              			 <label for="password2">パスワード確認用</label> <input
                    name="password2" type="password" id="password2"/><br />


                    <!-- ログインユーザーは自身の編集は不可 -->
			<c:if test="${loginUser.id == editUser.id}">
			<input type="hidden" name="branch" value="${editUser.branch_id}">
			<input type="hidden" name="position" value="${editUser.position_id}">
				<label for = "branch">支店</label><br>
					<select disabled="disabled">
						<c:forEach items="${branchList}" var="branch">
							<option value="${branch.id}">${branch.name}</option>
						</c:forEach>
					</select>

				<br>
				<label for="position">部署・役職</label><br>
					<select disabled="disabled">
						<c:forEach items="${positionList}" var="position">
							<option value="${position.id}">${position.name}</option>
						</c:forEach>
					</select><br><br>
                <input type="submit" value="変更" id="submit_button"/> <br />
            </c:if>

           <%--  <c:if test= "${loginUser.id != editUser.id}">
				<label for = "branch">支店</label>
					<select name="branch" >
						<option value="0" selected="selected" >選択してください
							<c:forEach items="${branchList}" var="branch">
								<option value="${branch.id}">${branch.name}</option>
							</c:forEach>
					</select>

					<label for="position">部署・役職</label>
					<select name="position" disabled="disabled">
						<c:forEach items="${positionList}" var="position">
							<option value="${position.id}">${position.name}</option>
						</c:forEach>
					</select>
                <input type="submit" value="変更" /> <br />
             --%>

          <!--  自身以外の編集するとき、支店と部署の値保持 -->
			<c:if test= "${loginUser.id != editUser.id}">
  					<label for="branch_id">支店</label><br />
						<select name="branch">
							<option value="0" >選択してください	</option>
					<!-- <input type="hidden" name="0" value="0" /> -->
						<c:forEach items="${branchList}" var="branch">
							<c:if test="${branch.id == editUser.branch_id}">
								<option value="${branch.id}" selected>${branch.name}</option>
							</c:if>
							<c:if test="${branch.id != editUser.branch_id}">
								<option value="${branch.id}">${branch.name}</option>
							</c:if>
						</c:forEach>
					</select>

  					<br /> <label for="position_id">部署･役職</label><br />
						<select name="position" >
								<option value="0" >選択してください</option>
							<c:forEach items="${positionList}" var="position">
							<c:if test="${position.id == editUser.position_id}">
								<option value="${position.id}" selected>${position.name}</option>
							</c:if>
							<c:if test="${position.id != editUser.position_id}">
								<option value="${position.id}">${position.name}</option>
							</c:if>
							</c:forEach>
						</select><br /><br />

                <input type="submit" value="更新"  id="submit_button"/> <br />
            </c:if>
           </form>
</div>

<br>
  <div class="copyright">Copyright(c)Kyosuke Gimba</div>

</body>
</html>