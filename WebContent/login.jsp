<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
</head>
<head>
    <link href="./css/login.css" rel="stylesheet" type="text/css">
</head>
<body>
</body>

			<br>
            <div class="title">社内掲示板</div>

             <c:if test="${ not empty errorMessages }">

                <div class="errorMessages">
                        <c:forEach items="${errorMessages}" var="message">
                           <br> <c:out value="${message}"/>
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <div class="main-contents">

            <form action="login" method="post">
                <label for="login_id">ログインID</label><br />
                <input name="login_id" id="login_id" value="${loginuser.login_id}"/>

               <p> <label for="password">パスワード</label><br />
                <input name="password" type="password" id="password"/> <br /></p>

                <input type="submit" value="ログイン"  id="submit_button" /> <br /><br />
            </form>
            <div class="copyright"> Copyright(c)Kyosuke Gimba</div>
        </div>

</html>