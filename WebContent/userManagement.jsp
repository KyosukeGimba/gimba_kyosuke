<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>

<head>
    <link href="./css/userManagement.css" rel="stylesheet" type="text/css">
</head>
<body><br><div class= "fixed">

	<div class="userManagement">ユーザー管理</div>

		<div class="header">
			<a href="logout" id="href" type="button">ログアウト</a>
		</div>
			<a href="signup" id="href" type="button">ユーザー新規登録</a>
			<br><br>
			<a href="./" id="href" type="button">ホームに戻る</a>


		 <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                        <c:forEach items="${errorMessages}" var="message">
                            <c:out value="${message}" />
                        </c:forEach>
                </div>
                <c:remove var="errorMessages" scope="session"/>
         </c:if>
         <br><br></div>

			<table border=1 class="account-name">
			<tr align="center" id="element"><th>名前</th><th>ログインID</th><th>支店</th><th>部署</th><th>設定</th><th>アカウント状況</th>

			<c:forEach items="${userList}" var="user">
			<tr><td align="center"><c:out value="${user.name}" /></td>
			<td align="center"><c:out value="${user.login_id}" /></td>
			<td align="center"><c:out value="${user.branch_name}" /></td>
			<td align="center"><c:out value="${user.position_name}" /></td>
            <td align="center"><a href="settings?id=${user.id}" class="button">編集</a></td>


            <td align="center"> <form action="Stop" method="post">
            <div class="onair">
            <input type="hidden"  name="id" value="${user.id}">
            	<c:if test= "${loginUser.id == user.id}">ログイン中</c:if>
            </div>

            	<c:if test="${user.is_stopped == 0 && loginUser.id != user.id}">
            	活動中
            	 	<input type="hidden"  name="is_stopped" value="1"  id="active">
            		<input type=submit value="停止"  onClick="return disp();"  id="active">
            	</c:if>


            	<c:if test="${user.is_stopped == 1 && loginUser.id != user.id}">
            	<label for="stop" id="stop">停止中</label>
            		<input type="hidden"  name="is_stopped" value="0" id="inactive">
            		<input type=submit value="復活"  onClick="return disp1();" id="inactive">
            	</c:if>

            	</form>
            </td>
  			 </c:forEach>
  			 </table>
		<br>

<a href="userManagement" id="href">上に戻る</a>

 <div class="copyright">Copyright(c)Kyosuke Gimba</div>
<script type="text/javascript">
<!--
function disp(){
	if(window.confirm('本当に停止しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
function disp1(){
	if(window.confirm('本当に復活しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
// -->
</script>
</body>
</html>