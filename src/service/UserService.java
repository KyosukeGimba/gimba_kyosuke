package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import beans.Position;
import beans.User;
import dao.UserDao;
import utils.CipherUtil;

public class UserService {
	public void register(User user) {
        Connection connection = null;

        //ユーザー登録をするためのメソッド
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.getUsers(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
	public User getUser(int userId) {
	    Connection connection = null;

	    //DAOからユーザーIDを受け取る
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User user = userDao.getUser(connection, userId);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public List<Branch> getBranch() {
		Connection connection = null;
		//支店情報を受け取る
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        List<Branch> branch = userDao.getBranch(connection);

	        commit(connection);

	        return branch;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public List<Position> getPosition() {
		Connection connection = null;
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        List<Position> position = userDao.getPosition(connection);

	        commit(connection);

	        return position;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public void update(User user) {

        Connection connection = null;
        //DAOから受け取った更新メソッドをservletに渡す
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public void userUpdate(int is_stopped, int id) {

		Connection connection = null;
		//停止、復活するためのメソッドをDAOから受け取り、Servletに渡す
		try {
			connection = getConnection();

			  UserDao userDao = new UserDao();
		      userDao.userUpdate(connection, is_stopped, id);

		      commit(connection);
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public User getLoginId(String login_id) {

	    Connection connection = null;
	    //ログインID
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User userList = userDao.getLoginId(connection,login_id);

	        commit(connection);

	        return userList;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public User getUserId(int userId) {
		// TODO 自動生成されたメソッド・スタブ

		  Connection connection = null;

		    //DAOからユーザーIDを受け取る
		    try {
		        connection = getConnection();

		        UserDao userDao = new UserDao();
		        User user = userDao.getUserId(connection, userId);

		        commit(connection);

		        return user;
		    } catch (RuntimeException e) {
		        rollback(connection);
		        throw e;
		    } catch (Error e) {
		        rollback(connection);
		        throw e;
		    } finally {
		        close(connection);
		    }
	}
}

