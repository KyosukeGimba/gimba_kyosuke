package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.NewPublish;
import beans.UserNewPublish;
import dao.NewPublishDao;
import dao.UserNewPublishDao;

public class NewPublishService {

	public void register(NewPublish newPublish) {
		//投稿の情報をDAOに送る
		Connection connection  = null;
		try {
			connection = getConnection();

			NewPublishDao newPublishDao = new NewPublishDao();
			newPublishDao.insert(connection, newPublish);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public List<UserNewPublish> getMessage(
			String firstDate, String finalDate,String searchCategory) {

		//投稿表示、、カテゴリー、期間絞込み
	    Connection connection = null;
	    try {
	        connection = getConnection();

	        List<UserNewPublish> ret = UserNewPublishDao.getUserNewPublish(connection, firstDate, finalDate, searchCategory);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public void delete(int id) {

		Connection connection  = null;
		//投稿削除のメソッドをDAOから受け取りServletに送る
		try {
			connection = getConnection();

			NewPublishDao newPublishDao = new NewPublishDao();
			newPublishDao.delete(connection, id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}