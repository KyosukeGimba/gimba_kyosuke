package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.UserDao;

public class UserManagementService {

	/*public void register(UserManagement userManagement) {

		//
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.getUsers(connection);

			  commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
		}
	}*/
	public List<User> getUsers() {

	    Connection connection = null;
	    //DAOで取得したすべてのユーザー情報をservletに送る
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        List<User> userList = userDao.getUsers(connection);

	        commit(connection);

	        return userList;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
	public User getUserId(int id) {

	    Connection connection = null;
	    //ユーザーIDをServletに送る
	    try {
	        connection = getConnection();

	        UserDao userDao = new UserDao();
	        User user = userDao.getUserId(connection, id);

	        commit(connection);

	        return user;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}

