package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.UserNewPublish;
import exception.SQLRuntimeException;

public class UserNewPublishDao {

	public static List<UserNewPublish> getUserNewPublish(
			Connection connection, String firstDate, String finalDate,String searchCategory) {

		//投稿表示をするための情報をユーザーに紐付けて取得する処理、カテゴリー検索、日付検索
	PreparedStatement ps = null;
	try {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		sql.append("publishes.id as id, ");
        sql.append("publishes.text as text, ");
        sql.append("publishes.user_id as user_id, ");
        sql.append("publishes.title as title, ");
        sql.append("publishes.category as category, ");
        sql.append("users.name as name, ");
        sql.append("publishes.created_date as created_date ");
        sql.append("FROM publishes ");
        sql.append("INNER JOIN users ");
        sql.append("ON publishes.user_id = users.id ");
        //期間設定
//        if(firstDate !=null && (StringUtils.isBlank(firstDate) != true
//        		&& finalDate !=null && (StringUtils.isBlank(finalDate) != true))) {
        sql.append("where publishes.created_date between ? and ? ");
//        }
        if(searchCategory != null && (StringUtils.isBlank(searchCategory) != true)) {
        	//カテゴリー
            sql.append("and category like ?");
        }
        sql.append("ORDER BY publishes.created_date DESC ");

        ps = connection.prepareStatement(sql.toString());

        //日付が入っていれば、セット
        if(firstDate !=null && (StringUtils.isBlank(firstDate) != true
        		&& finalDate !=null && (StringUtils.isBlank(finalDate) != true))) {

        	ps.setString(1, firstDate + " 00:00:00");
        	ps.setString(2, finalDate + " 23:59:59");
        }

        //カテゴリーが入っていれば、部分一致で
        if(searchCategory != null && (StringUtils.isBlank(searchCategory) != true)) {
    	   ps.setString(3, "%" + searchCategory + "%");
        }

        //System.out.println(ps.toString());
        ResultSet rs = ps.executeQuery();
        List<UserNewPublish> ret = toUserNewPublishList(rs);

        return ret;
        } catch (SQLException e) {
        	throw new SQLRuntimeException(e);
        } finally {
        		close(ps);
        }
	}
	private static List<UserNewPublish> toUserNewPublishList(ResultSet rs) throws SQLException {

		//投稿をするための情報をリストに入れる処理
		List<UserNewPublish> ret = new ArrayList<UserNewPublish>();
		try {
			while (rs.next()) {
				String name = rs.getString("name");
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserNewPublish newPublish = new UserNewPublish();
				// newPublish.setAccount(account);
				newPublish.setName(name);
				newPublish.setId(id);
				newPublish.setUserid(userId);
				newPublish.setText(text);
				newPublish.setTitle(title);
				newPublish.setCategory(category);
				newPublish.setCreated_date(createdDate);

				ret.add(newPublish);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}


