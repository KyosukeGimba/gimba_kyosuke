package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.NewPublish;
import exception.SQLRuntimeException;

public class NewPublishDao {

	public void insert(Connection connection, NewPublish newPublish) {

		//記事の情報をDBに入れる処理
		PreparedStatement ps = null;
	    try {
	    	StringBuilder sql = new StringBuilder();
	    	sql.append("INSERT INTO publishes ( ");
            sql.append("user_id");
            sql.append(", title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // text
            sql.append(", ?"); // text
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, newPublish.getUserId());
            ps.setString(2, newPublish.getTitle());
            ps.setString(3, newPublish.getText());
            ps.setString(4, newPublish.getCategory());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public void delete(Connection connection, int id) {

		//投稿を削除するメソッド
		 PreparedStatement ps = null;
	     try {
	    	 StringBuilder sql = new StringBuilder();
	    	 sql.append("DELETE FROM publishes");
	    	 sql.append(" where");
	    	 sql.append(" id = ?");

	    	 ps = connection.prepareStatement(sql.toString());

	    	 ps.setInt(1, id);
	    	// System.out.println(ps);

	         ps.executeUpdate();
	     } catch (SQLException e) {
	    	 throw new SQLRuntimeException(e);
	     } finally {
	    	 close(ps);
	     }
	}
}
