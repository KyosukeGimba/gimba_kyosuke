package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void getUsers(Connection connection, User user) {

		//ユーザー情報をDBに登録する処理
		PreparedStatement ps = null;
		try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("  ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", 0");
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1,user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            //System.out.println(ps.toString());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	public User getUser(Connection connection, String login_id, String password) {

		//ログインIDとパスワードを取得する処理
		PreparedStatement ps = null;
		try {
			String sql =  "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);
		    ps.setString(2, password);

		   // System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
    }
    private List<User> toUserList(ResultSet rs) throws SQLException {

    	//リストにユーザー情報をまとめる処理
        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int branch_id = rs.getInt("branch_id");
            	int position_id = rs.getInt("position_id");
            	int is_stopped = rs.getInt("is_stopped");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setPassword(password);
                user.setBranch_id(branch_id);
            	user.setPosition_id(position_id);
            	user.setIs_stopped(is_stopped);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<User> getUsers(Connection connection) {

    	//ユーザー情報を支店と紐付けて取得する処理
    	PreparedStatement ps = null;
    	//String sql = "select * from users;";
    	try {
    		StringBuilder sql = new StringBuilder();

    		sql.append("SELECT ");
    		sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("users.password as password, ");
            sql.append("users.is_stopped as is_stopped, ");
            sql.append("users.branch_id as branch_id, ");
            sql.append("users.position_id as position_id, ");
            sql.append("branches.name as branch_name, ");
            sql.append("positions.name as position_name ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("ORDER BY branch_id, position_id, id ASC; ");


    		ps = connection.prepareStatement(sql.toString());
    	  //  System.out.println(ps);
    		ResultSet rs = ps.executeQuery();
    	    List<User> userfList = toUserfList(rs);

    	    return userfList;
    	} catch (SQLException e) {
    	    throw new SQLRuntimeException(e);
    	} finally {
    	    close(ps);
    	}
    }
    private List<User> toUserfList(ResultSet rs) throws SQLException {

    	//ユーザー情報に加え、支店、部署の情報もリストに加える処理
        List<User> ret = new ArrayList<User>();
        try {
        	while (rs.next()) {
        		int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int is_stopped = rs.getInt("is_stopped");
                int branch_id = rs.getInt("branch_id");
            	int position_id = rs.getInt("position_id");
            	String branch_name = rs.getString("branch_name");
            	String position_name = rs.getString("position_name");

            	User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setPassword(password);
                user.setBranch_id(branch_id);
            	user.setPosition_id(position_id);
            	user.setIs_stopped(is_stopped);
            	user.setBranch_name(branch_name);
            	user.setPosition_name(position_name);

                ret.add(user);
            }
            return ret;
        } finally {
        	close(rs);
        }
    }
    public User getUser(Connection connection, int id) {

    	//ユーザーIDを取得する処理
        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, User user) {

    	//ユーザー情報を更新するためのメソッド
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  name = ?");
            sql.append(", login_id = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", password = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getName());
            ps.setString(2, user.getLogin_id());
            ps.setInt(3, user.getBranch_id());
            ps.setInt(4, user.getPosition_id());
            ps.setString(5, user.getPassword());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void userUpdate(Connection connection, int is_stopped, int id) {

    	//停止、復活させるためのメソッド
    	PreparedStatement ps = null;
    	try {
    		String sql = "update users set is_stopped = ? where id = ?;";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, is_stopped);
    		ps.setInt(2, id);

    	//	System.out.println(ps);
    		int count = ps.executeUpdate();
            if (count == 0) {
                 throw new NoRowsUpdatedRuntimeException();
            }
    	 } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }
    public List<Branch> getBranch(Connection connection) {

    	//支店を取得する処理
    	PreparedStatement ps = null;
    	//String sql = "select * from users;";
    	try {
    		String sql = "select * from branches;";

    		ps = connection.prepareStatement(sql);

		   // System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
	        List<Branch> branchList = toBranchList(rs);
	        return branchList;

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
    }
    private List<Branch> toBranchList(ResultSet rs) throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String name = rs.getString("name");

                Branch branch = new Branch();
                branch.setId(id);
                branch.setName(name);
                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public List<Position> getPosition(Connection connection) {
    	PreparedStatement ps = null;
    	//String sql = "select * from users;";
    	try {
    		String sql = "select * from positions;";

    		ps = connection.prepareStatement(sql);

		   // System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
	        List<Position> positionList = toPositionList(rs);
	        return positionList;

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
    }
    private List<Position> toPositionList(ResultSet rs) throws SQLException {

        List<Position> ret = new ArrayList<Position>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String name = rs.getString("name");

                Position position = new Position();
                position.setId(id);
                position.setName(name);
                ret.add(position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
   /* public List<User> getLoginId(Connection connection) {
    	PreparedStatement ps = null;
    	//String sql = "select * from users;";
    	try {
    		String sql = "select * from users where login_id = ?;";

    		ps = connection.prepareStatement(sql);

		   // System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
	        List<User> loginIdList = toLoginIdList(rs);
	        return loginIdList;

	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
    }*/
    public User getLoginId(Connection connection, String login_id) {

		PreparedStatement ps = null;
		try {
			String sql =  "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);

		   // System.out.println(ps.toString());

			ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
    }
   /* private List<User> toLoginIdList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                String loginId = rs.getString("login_id");

                User user = new User();
                user.setLogin_id(loginId);
                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }*/

    public User getUserId(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}

