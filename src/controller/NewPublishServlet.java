package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.NewPublish;
import beans.User;
import service.NewPublishService;

@WebServlet(urlPatterns = { "/newPublish" })
public class NewPublishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request,
	    	HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("publish.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
	        HttpServletResponse response) throws IOException, ServletException {

		//ログインユーザーのセッションを取得して、投稿用インスタンスに情報とIDを入れる
		HttpSession session = request.getSession();
		List<String> messageList = new ArrayList<String>();
		User user = (User) session.getAttribute("loginUser");

		NewPublish newPublish = new NewPublish();
		newPublish.setTitle(request.getParameter("title"));
		newPublish.setText(request.getParameter("text"));
		newPublish.setCategory(request.getParameter("category"));
		newPublish.setUserId(user.getId());

		//バリデーションOKだったら、登録してホームへ
		if(isValid(request, messageList) == true) {
			new NewPublishService().register(newPublish);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messageList);
			request.setAttribute("newPublish", newPublish);
			request.getRequestDispatcher("publish.jsp").forward(request, response);
		}
	}
	 private boolean isValid(HttpServletRequest request, List<String> messageList) {

	 String title = request.getParameter("title");
	 String text = request.getParameter("text");
	 String category = request.getParameter("category");

	 if (StringUtils.isBlank(title) == true) {
		 messageList.add("タイトルを入力してください");
	 }
	 if(StringUtils.isBlank(title) != true && 30 <= title.length()) {
		 messageList.add("タイトルは30文字以下で入力してください");
	 }
	 if (StringUtils.isBlank(text) == true) {
		 messageList.add("本文を入力してください");
	 }
	 if(StringUtils.isBlank(text) != true && 1000 <= text.length()) {
		 messageList.add("本文は1000文字以下で入力してください");
	 }
	 if (StringUtils.isBlank(category) == true) {
		 messageList.add("カテゴリーを入力してください");
	 }
	 if(StringUtils.isBlank(category) != true && 10 <= category.length()) {
		 messageList.add("カテゴリーは10文字以下で入力してください");
	 }
	 if(messageList.size() == 0) {
	      return true;
	 } else {
	     return false;
	 }
}
//	 boolean isShowMessageForm;
//		 if (messageList != null){
//			 isShowMessageForm = true;
//		 } else {
//			 isShowMessageForm = false;
//		 }
//		 request.setAttribute("isShowMessageForm", isShowMessageForm);
//		 request.getRequestDispatcher("/NewPublish.jsp").forward(request, response);
}


