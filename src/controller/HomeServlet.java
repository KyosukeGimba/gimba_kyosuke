package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.UserComment;
import beans.UserNewPublish;
import service.CommentService;
import service.NewPublishService;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String searchCategory = request.getParameter("searchCategory");
		String firstDate = request.getParameter("firstDate");
		String finalDate = request.getParameter("finalDate");

		if(firstDate == null || (StringUtils.isBlank(firstDate) == true)){
			firstDate = "2018-05-01";
		}else{
			request.setAttribute("firstDate", firstDate);
		}
		if(finalDate == null || (StringUtils.isBlank(finalDate) == true)){
//			finalDate = "2018-05-31";
			//今日の日付を取得する処理
			Date today = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			finalDate = sdf.format(today);
		}else{
			request.setAttribute("finalDate", finalDate);
		}
		//カテゴリーが入力されていたら、保持する処理
		if(searchCategory != null && (StringUtils.isBlank(searchCategory) == false)){
			request.setAttribute("searchCategory", searchCategory);
		}
		//if(searchCategory == null && firstDate == null && finalDate == null) {

		//}

	/*	List<String> searchList = new ArrayList<String>();
		searchList.add(searchCategory);
		searchList.add(firstDate);
		searchList.add(finalDate);

		request.setAttribute(finalDate, searchList);*/



		//System.out.println(firstDate);

//		if (StringUtils.isBlank(title) == true) {
//			 messageList.add("タイトルを入力してください");


//		 }


		 //投稿表示
		 List<UserNewPublish> newPublishes  = new NewPublishService().getMessage(firstDate, finalDate,searchCategory);
		 request.setAttribute("newPublishes",  newPublishes);


		 //コメント表示
		 List<UserComment> comments = new CommentService().getComment();
		 request.setAttribute("comments",  comments);
		 request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
/*

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}*/
}
