package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.UserService;


/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
		//支店、部署などをプルダウンに自動表示
		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Position> positionList = new UserService().getPosition();
		request.setAttribute("positionList",positionList);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
	}
	 @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {
		//支店、部署などをプルダウンに自動表示の保持
		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Position> positionList = new UserService().getPosition();
		request.setAttribute("positionList",positionList);


        List<String> messages = new ArrayList<String>();

        User user = new User();
        user.setLogin_id(request.getParameter("login_id"));
        user.setName(request.getParameter("name"));
        user.setPassword(request.getParameter("password1"));
        user.setBranch_id(Integer.parseInt(request.getParameter("branch")));
        user.setPosition_id(Integer.parseInt(request.getParameter("position")));
       // user.setIs_stopped(Integer.parseInt(request.getParameter("is_stopped")));

        HttpSession session = request.getSession();
        //バリデーションOKだったら登録
        if (isValid(request, messages) == true) {
            new UserService().register(user);

            response.sendRedirect("userManagement");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("user",user);

            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
	 }
	 private boolean isValid(HttpServletRequest request, List<String> messages) {
		 String name = request.getParameter("name");
		 String login_id = request.getParameter("login_id");
	     String password1 = request.getParameter("password1");
	     String password2 = request.getParameter("password2");
	     int branchId = Integer.parseInt(request.getParameter("branch"));
	     int positionId = Integer.parseInt(request.getParameter("position"));

	     User loginIdList = new UserService().getLoginId(login_id);
			request.setAttribute("loginIdList",loginIdList);


	     if(StringUtils.isBlank(name) == true) {
	    	 messages.add("名称を入力してください");
	     } else if( name.length() >= 10) {
	    	 messages.add("名称は10文字以下で入力してください");
	     }
	     if(StringUtils.isBlank(login_id) == true) {
	    	 messages.add("ログインIDを入力してください");
	     } else if(!login_id.matches("^[0-9a-zA-Z]{6,20}$")) {
		     messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
		 }
	     if(loginIdList != null) {
	    	 messages.add("そのログインIDは使用できません");
	     }

	     if(StringUtils.isBlank(password1) == true) {
	    	 messages.add("パスワードを入力してください");
	     } else if(!password1.matches("^[a-zA-Z0-9\\p{Punct}]{6,20}$")) {
	    	 messages.add("パスワードは半角英数字記号6文字以上20文字以下で入力してください");
	     } else if(StringUtils.isBlank(password2) == true) {
	    	 messages.add("確認用のパスワードを入力してください");
	     } else if(StringUtils.isBlank(password1) != true && !password1.matches(password2)) {
	    	 messages.add("パスワードと確認パスワードが一致していません");
	     }
	     if(branchId == 0 && positionId != 0) {
	    	 messages.add("支店を選択してください");
	     } else if(branchId != 0 && positionId == 0) {
	    	 messages.add("部署･役職を選択してください");
	     } else if(branchId == 0 && positionId == 0) {
	    	 messages.add("支店と部署･役職を選択してください");
	     } else if(branchId != 1 && (positionId == 1 || positionId == 2) == true) {
	        messages.add("支店と部署･役職の組み合わせが不正です");
	     } else if(branchId == 1 && positionId == 3) {
	    	 messages.add("支店と部署･役職の組み合わせが不正です");
	     }
	     if (messages.size() == 0) {
	      	return true;
	     } else {
			return false;
	     }
	 }
}

