package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.UserService;

/**
 * Servlet implementation class SettingsServlet
 */
@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		//支店、部署などをプルダウンに自動表示
		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Position> positionList = new UserService().getPosition();
		request.setAttribute("positionList",positionList);

		//数字以外は不正
		List<String> messages = new ArrayList<String>();
		if(!String.valueOf(request.getParameter("id")).matches("^[0-9]+$") ) {
			messages.add("不正なアクセスです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");
			return;
		}
		//HttpSession session = request.getSession();
//	        User users = (User) session.getAttribute("users");
		//ユーザー編集

		//ユーザー編集画面の表示 空は不正
		int userId = Integer.parseInt(request.getParameter("id"));
		User editUser = new UserService().getUser(userId);
		if(editUser == null) {
			messages.add("不正なアクセスです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");
			return;
		}
		request.setAttribute("editUser", editUser);
		request.setAttribute("before_id", editUser.getLogin_id());
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		//支店、部署などをプルダウンに自動表示の保持
		List<Branch> branchList = new UserService().getBranch();
		request.setAttribute("branchList",branchList);

		List<Position> positionList = new UserService().getPosition();
		request.setAttribute("positionList",positionList);


		//ユーザー設定変更
		List<String> users = new ArrayList<String>();
		HttpSession session = request.getSession();
	    User editUser = getEditUser(request);
		session.setAttribute("editUser", editUser);
		/*String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		if(StringUtils.isEmpty(password1) && StringUtils.isEmpty(password2)) {
	        session.setAttribute("password1",password1);
		}*/

		//バリデーションOKだったら、更新する処理
		if (isValid(request, users) == true) {
			try {
				new UserService().update(editUser);

			} catch (NoRowsUpdatedRuntimeException e) {
				session.setAttribute("errorMessages", users);
	            request.setAttribute("editUser", editUser);
	            response.sendRedirect("settings");
	            return;
	        }
	            session.setAttribute("users", editUser);
	            response.sendRedirect("userManagement");
	    } else {
	    	request.setAttribute("before_id",request.getParameter("before_id"));
	    	session.setAttribute("errorMessages", users);
	        request.setAttribute("editUser", editUser);
	        request.getRequestDispatcher("settings.jsp").forward(request, response);
	    }

	}
	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {
		//ユーザー設定変更用の要素をJSPから受け取る
		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setPassword(request.getParameter("password1"));
		editUser.setBranch_id(Integer.parseInt(request.getParameter("branch")));
		editUser.setPosition_id(Integer.parseInt(request.getParameter("position")));
		editUser.setLogin_id(request.getParameter("login_id"));
		return editUser;

	}
	private boolean isValid(HttpServletRequest request, List<String> users)
			throws IOException, ServletException {

		String name = request.getParameter("name");
		String before_id = request.getParameter("before_id");
		String login_id = request.getParameter("login_id");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		int positionId = Integer.parseInt(request.getParameter("position"));
		int branchId = Integer.parseInt(request.getParameter("branch"));;
		//request.setAttribute("password1",password1);

		User userList = new UserService().getLoginId(login_id);
		request.setAttribute("loginIdList",userList);
//		User user = new UserService().getUser(userId);
		User editUser = getEditUser(request);

		if(StringUtils.isBlank(name) == true) {
	    	 users.add("名称を入力してください");
	     } else if( name.length() >= 10) {
	    	 users.add("名称は10文字以下で入力してください");
	     }
	     if(StringUtils.isBlank(login_id) == true) {
	    	 users.add("ログインIDを入力してください");
	     } else if(!login_id.matches("^[0-9a-zA-Z]{6,20}$")) {
	    	 users.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
		 }
        if(!before_id.equals(login_id)){
        	if(userList != null && editUser.getId() != userList.getId()){
        		users.add("そのログインIDは使用できません");
        	}
        }


       /* if(StringUtils.isBlank(password1) == true) {
        	users.add("パスワードを入力してください");
        }
        if(StringUtils.isBlank(password2) == true) {
 	        users.add("確認用パスワードを入力してください");*/
        //}


        if(StringUtils.isBlank(password1) != true &&
	    		 !password1.matches("^[a-zA-Z0-9\\p{Punct}]{6,20}$")) {
	    	 users.add("パスワードは半角英数字記号6文字以上20文字以下で入力してください");
	    } else if(!StringUtils.isBlank(password1) && !password1.matches(password2) || !StringUtils.isBlank(password2) && !password2.matches(password1)) {
	        	users.add("パスワードと確認パスワードが一致していません");
	    } else
        if(!StringUtils.isEmpty(password1) && !StringUtils.isEmpty(password2)) {
        	if(StringUtils.isBlank(password1) && StringUtils.isBlank(password2)) {
        	users.add("パスワードは空白で入力することはできません");
        	}
        } else if(!StringUtils.isEmpty(password1) || !StringUtils.isEmpty(password2)) {
        	if(StringUtils.isBlank(password1) || StringUtils.isBlank(password2)) {
        	users.add("パスワードは空白で入力することはできません");
        	}
        }

	     if(branchId == 0 && positionId != 0) {
	    	 users.add("支店を選択してください");
	     }
	     if(branchId != 0 && positionId == 0) {
	    	 users.add("部署･役職を選択してください");
	     }
	     if(branchId == 0 && positionId == 0) {
	    	 users.add("支店と部署･役職を選択してください");
	     }
        if(branchId != 1 && (positionId == 1 || positionId == 2) == true) {
        	users.add("支店と部署･役職の組み合わせが不正です");
        }
	     if(branchId == 1 && positionId == 3) {
	    	 users.add("支店と部署･役職の組み合わせが不正です");
	     }
	  /*  if(!password1.matches("{6,20}""^[a-zA-Z0-9\-\\.\\/\\^\\@\]{6,20}$")){
	    	users.add("パスワードは6文字以上20文字以下で登録してください");
	    }*/

        /*if(password1 != password2 == true) {
        	users.add("パスワードと確認パスワードが一致していません。");
        }*/
        if (users.size() == 0) {
            return true;
        } else {
            return false;
	    }
	}
}
