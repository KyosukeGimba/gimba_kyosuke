package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.LoginService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	 protected void doGet(HttpServletRequest request,
	    		HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
		}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		//JSPから入力された値を受け取る
		String login_id = request.getParameter("login_id");
	    String password = request.getParameter("password");

       // System.out.println(login_id);

        LoginService loginService = new LoginService();
        User user = loginService.login(login_id, password);

        HttpSession session = request.getSession();
        User loginUser = getLoginUser(request);

        if (user != null) {
        	session.setAttribute("loginUser", user);

            if(user.is_stopped != 0) {
            	//ユーザーが停止されていた時の処理
            	List<String> messages = new ArrayList<String>();
        		messages.add("ログインに失敗しました");
        		session.setAttribute("errorMessages", messages);
        		request.setAttribute("loginuser",loginUser);
        		request.getRequestDispatcher("login.jsp").forward(request, response);
        		return;
        	}
        } else {
    		List<String> messages = new ArrayList<String>();
    		messages.add("ログインに失敗しました");
    		session.setAttribute("errorMessages", messages);
    		request.setAttribute("loginuser",loginUser);
    		request.getRequestDispatcher("login.jsp").forward(request, response);
    		return;
        }
        response.sendRedirect("./");
	}
	private User getLoginUser(HttpServletRequest request)
		throws IOException, ServletException {

		User loginUser = new User();
		loginUser.setLogin_id(request.getParameter("login_id"));
		request.setAttribute("loginuser",loginUser);
		return loginUser;
	}
}