package controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.UserManagementService;

@WebServlet(urlPatterns = { "/userManagement" })
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		//全ユーザー情報が入ったユーザーリストを保持し、serviceに渡す
		List<User> userList  = new UserManagementService().getUsers();
		request.setAttribute("userList", userList);
		request.getRequestDispatcher("userManagement.jsp").forward(request, response);
	}

// 	private boolean isValid(HttpServletRequest request, List<User> toUserList) {
//
// 		 String name = request.getParameter("Name");
//		 String id = request.getParameter("Id");
//
//		 return true;
// 	}
/* 	@Override
 	protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {

		 request.getRequestDispatcher("userManagement.jsp").forward(request, response);
 	}*/
}
